

void writeEEPROMuslong(unsigned int eeaddress, unsigned long data)
{
  byte iL1 = data >> 24;
  byte iL2 = data >> 16;
  byte iH1 = data >> 8;
  byte iH2 = data;
  writeEEPROMbyte(eeaddress, iL1);
  writeEEPROMbyte(eeaddress + 1, iL2);
  writeEEPROMbyte(eeaddress + 2, iH1);
  writeEEPROMbyte(eeaddress + 3, iH2);
}

void writeEEPROMint(unsigned int eeaddress, int data)
{
  byte iL = data >> 8;
  byte iH = data;
  writeEEPROMbyte(eeaddress, iL);
  writeEEPROMbyte(eeaddress + 1, iH);
}
 
void writeEEPROMbyte(unsigned int eeaddress, byte data ) 
{
  Wire.beginTransmission(disk1);
  Wire.write((int)(eeaddress >> 8));   // MSB
  Wire.write((int)(eeaddress & 0xFF)); // LSB
  Wire.write(data);
  Wire.endTransmission();
  delay(5);
}

unsigned long readEEPROMuslong(unsigned int eeaddress)
{
  byte iL1 = readEEPROMbyte(eeaddress);
  byte iL2 = readEEPROMbyte(eeaddress + 1);
  byte iH1 = readEEPROMbyte(eeaddress + 2);
  byte iH2 = readEEPROMbyte(eeaddress + 3);
  unsigned long number = iL1;
  number = number << 8;
  number = number + iL2;
  number = number << 8;
  number = number + iH1;
  number = number << 8;
  number = number + iH2;
  
  return number;
} 
 
int readEEPROMint(unsigned int eeaddress)
{
  byte iL = readEEPROMbyte(eeaddress);
  byte iH = readEEPROMbyte(eeaddress + 1);
  int number = iL;
  number = number << 8;
  number = number + iH;
  return number;
}
 
byte readEEPROMbyte(unsigned int eeaddress ) 
{
  byte rdata = 0xFF; 
  Wire.beginTransmission(disk1);
  Wire.write((int)(eeaddress >> 8));   // MSB
  Wire.write((int)(eeaddress & 0xFF)); // LSB
  Wire.endTransmission(); 
  Wire.requestFrom(disk1 , 1); 
  if (Wire.available()) rdata = Wire.read(); 
  return rdata;
}
