void updateTime() {
  Wire.beginTransmission(0x68); // 0x68 is DS3231 device address
  Wire.write((byte)0); // start at register 0
  Wire.endTransmission();
  
  Wire.requestFrom(0x68, 7); // request three bytes (seconds, minutes, hours)  
  while(Wire.available())
  { 
    byte seconds = bcdToDec(Wire.read()); // get seconds
    byte minutes = bcdToDec(Wire.read()); // get minutes
    byte hours = bcdToDec(Wire.read());   // get hours
    byte dayWeek = bcdToDec(Wire.read());
    byte dayMonth = bcdToDec(Wire.read());
    byte month = bcdToDec(Wire.read());
    byte year = bcdToDec(Wire.read());
 
    //seconds = (((seconds & 0b11110000)>>4)*10 + (seconds & 0b00001111)); // convert BCD to decimal
    //minutes = (((minutes & 0b11110000)>>4)*10 + (minutes & 0b00001111)); // convert BCD to decimal
    //hours = (((hours & 0b00100000)>>5)*20 + ((hours & 0b00010000)>>4)*10 + (hours & 0b00001111)); // convert BCD to decimal (assume 24 hour mode)

    timeReference.seconds = (int)seconds;
    timeReference.minutes = (int)minutes;
    timeReference.hours = (int)hours;
    timeReference.dayWeek = (int)dayWeek;
    timeReference.dayMonth = (int)dayMonth;
    timeReference.month = (int)month;
    timeReference.year = (int)year;    
  }
}

void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(0x68);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
  lcd.print("Time updated");
  delay(2000);
}
