void mainAlarm() {
  /*  
  //TODO Make these 3 options editable in menus and settings
  if ((duration > 300)&&(lightingStrength==-1)) {
    displaysOn = false;
  }

  if ((duration < 300)&&(duration > 298)) {
      lamp = false;
      alarmRunning = true;
      alarmStrobe();
  }
  
  if (duration < 298) {
    if (duration > 290){
      lamp = true;
      alarmRunning = false;
      toneAC();
    }
    displaysOn = true;  
  }
    

  if (duration == 10) lamp = false;
  
  */
  
  if (alarmReference.hours == timeReference.hours) {
    if (alarmReference.minutes == timeReference.minutes) {
      if (timeReference.seconds < 2) {
        lightingStrength = 0;
        displaysOn = true;
        lcd.backlight();    //Cheating
        lcd2.backlight();
        lcd.clear();
        lcd2.clear();
        alarmGoing();
      }
    }
  }  
}

void snoozeAlarm() {
  if (timeReference.seconds != oldSeconds) {
    oldSeconds = timeReference.seconds;
    
    snoozeReference.seconds--;
    if (snoozeReference.seconds < 0) {
      snoozeReference.seconds = 59;
      snoozeReference.minutes--;
      if (snoozeReference.minutes < 0) {
        snoozeReference.hours -1;
        if (snoozeReference.hours < 0) snoozeReference.hours = 0;          
      }
    }    
  }

  if (snoozeReference.hours == 0) {
    if (snoozeReference.minutes == 0) {
      if (snoozeReference.seconds == 0) {
        timerOn = false;
        lightingStrength = 0;
        displaysOn = true;
        lcd.backlight();    //Cheating
        lcd2.backlight();
        lcd.clear();
        lcd2.clear();
        alarmGoing();
      }
    }
  }
}



void alarmGoing() {
  while (runLevel != 10) {
    
    buttonInput();
    
    alarmRunning = true;
    lcd.setCursor(0,0);
    lcd.print("Wakeup beautiful :) ");
    alarmStrobe();

    if (inputBuffer > 0) {
      if (lightingStrength < 0) lightingStrength = 0;
      alarmRunning = false;
      menuReprint = true;
      menuLocation = 0;
      runLevel = 10;
      toneAC();
    }
  }
}

