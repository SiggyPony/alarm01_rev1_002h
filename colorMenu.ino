void colorMenu()
{
  if (menuReprint)
  {
    menuReprint = false;
    lcd.clear();
    specialMessageOne();
    if (menuLocation == 0)
    {
      lcd.setCursor(0,0);
      lcd.print("Color ->");
      lcd.print(colors[lightingColor]);
    }
    
    //Menu for Control Points   
    if (menuLocation == 1)
    {
      lcd.setCursor(0,0);
      lcd.print("Strength");
      lcd.print(" ->");
      lcd.print(lightingStrength);
      lcd.print("%");
    }
    
    if (menuLocation == 2)
    {
      lcd.setCursor(0, 0);
      lcd.print("Light   ");
      if (lamp == true) lcd.print("[On]");
      if (lamp == false) lcd.print("[Off]");
      
    }
    
    if (menuLocation == 3)
    {
      lcd.setCursor(2, 0);
      lcd.write((uint8_t)1);
      lcd.print(" - Ma");
      lcd.print("in Menu ");

    }

  }
 
  if (inputBuffer > 0)
  {
    if (menuLocation == 0)
    {
      if (inputBuffer == 2)
      {        
        lightingColor+=1;
        if (lightingColor > 9) lightingColor = 9;
      }
      if (inputBuffer == 3)
      {
        lightingColor-=1;
        if (lightingColor < 0) lightingColor = 0;
      }  
    }
    
    
    if (menuLocation == 1)
    {
      if (inputBuffer == 2)
      {        
        lightingStrength+=10;
        if (lightingStrength > 100) {
          lightingStrength = 100;          
        }
      }
      if (inputBuffer == 3)
      {
        lightingStrength-=10;
        if (lightingStrength < 0) lightingStrength = 0; 
      }  
    }
            
    if (menuLocation == 2)
    {
      if (inputBuffer == 2)
      {        
        lamp=true;
      }
      if (inputBuffer == 3)
      {
        lamp=false;
      }  
    }
    
    if (menuLocation == 3)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 0;
      }
    }
    
    
    
    
        //Move through menus.
    if (inputBuffer == 1)
    {      
      menuLocation--;
      if (menuLocation < 0) menuLocation = 0;
    }
    
    if (inputBuffer == 4)
    {
      menuLocation++;
      
      //Menu length for Control Points  
      if (menuLocation > 3) menuLocation = 3;
    }
    
    menuReprint = true;
    inputBuffer = 0; 
  }
}
