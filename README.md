# README #

This is a snapshot of a development version of the software running on my personal alarm clock.
It is essentially in this snapshot an alarm clock that has alarm and snooze functions with a build in RGB nightlight.

I built it because I have trouble getting out of bed. Another version of the code requires the input of maths solutions to turn off, this one is nicer.

This snapshot is quite a few iterations and rounds of refactoring out of date. 
It uses a sort of system for menu's and buttons that I wrote myself about 5 years ago and never 
really saw a reason to change especially considering the personal use 
of the project. 

I have higher standards for my code when it is a non personal project, production device or is work related.

It provides a basic 4 button interface with input varying on the amount of time the button is held down.
I.e. Longer push for faster data entry etc.
2 extra buttons give seperate functionality which is broken in this snapshot. One turns the displays on and off
and one turns on patterns for the RGB led's
It has two 2004 Char LCD's, a panel of RGB Led's, a buzzer, it has two seperate Eprom chips which the full
functionality of which hasn't been implemented in this snapshot. 

I more or less only add features or work on it when I think of something I want my alarm clock to do that it doesn't already.
I soldiered it's internals, including it's switch mode PSU and built it's case myself.

It's a comparitively small project to the two other projects I have. But those are private and I would only show them in a job interveiw :)