#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <toneAC.h>

#define lightPin 2
#define NUMPIXELS 16

#define alive 8

 
//Main Keyboard
#define buttonNo A0
#define buttonUp A1
#define buttonDown A2
#define buttonYes A3
#define buttonDisplay 7
#define buttonLightType 6

//define I2C periferal addresses

#define disk1 0x50    //Address of 24LC32A eeprom chip

//TODO Why disk2 not work?

#define disk2 0x56    //Address of 24LC32A eeprom chip
#define rTC 0x68      //Address of DS3231 RTC
#define screenOne 0x27     //Address of LCD one
#define screenTwo 0x26     //Address of LCD one

//define ePROM map

#define epRewards 1


/*
//Test Keyboard
#define buttonNo A3
#define buttonUp A2
#define buttonDown A1
#define buttonYes A0
*/

unsigned char colorsRGB[][3] = {
  {255,0,0},
  {255,128,0},
  {255,255,0},
  {0,255,0},
  {0,255,255},
  {0,0,255},
  {128,0,255},
  {255,0,128},
  {255,106,180},  
  {255,255,255}};
  
  
String colors[] = {
  "Red",
  "Orange",
  "Yellow",
  "Green",
  "Cayn",
  "Blue",
  "Purple",
  "Magnenta",
  "Pink",
  "White"};
  

byte upArrow[8] = {
  B00100,
  B01110,
  B10101,
  B00100,
  B00100,
  B00100,
  B00000,
  B00000
};


byte downArrow[8] = {
  B00000,
  B00000,
  B00100,
  B00100,
  B00100,
  B10101,
  B01110,
  B00100
};

byte love[8] = {
  B00000,
  B01010,
  B10101,
  B10001,
  B01010,
  B00100,
  B00000,
  B00000
};


//struct for Straight Mode Data Storage
struct DateTime {
  int seconds;
  int minutes;
  int hours;
  int dayWeek;
  int dayMonth;
  int month;
  int year;
};

struct SimpleTime {
  int seconds;
  int minutes;
  int hours;
  

};
/*
struct ab {
  long a;
  long b;
};
*/
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, lightPin, NEO_GRB + NEO_KHZ800);


// initialize the library with the numbers of the interface pins
LiquidCrystal_I2C lcd(screenOne, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
LiquidCrystal_I2C lcd2(screenTwo, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address


int runLevel = 0;

/*
0 = main menu
1 = alarm settings
2 = cute light
51 = Alarm
*/

DateTime timeReference = {0,0,0};
DateTime timeUpdateReference = {0, 30, 12};
SimpleTime alarmReference = {0,0,11}; //Should be 0,0,0
SimpleTime snoozeReference = {0,10,0};
int oldSeconds = 0;

boolean updated = true;



int lightingColor = 0;
int lightingStrength = 10;
int alarmSelection = 0;



boolean lamp = false;
int lampStyle = 0;  //0 = default, 1 = ripple
int rippleValue[NUMPIXELS];
int rippleModOne = 0;    //Values for modifying ripple
int rippleModTwo = 0;
boolean alarmRunning = false;

boolean timerOn = false;
boolean alarmOn = false;




 

int inputBuffer = 0;  //1 = No, 2 = Up, 3 = Down, 4 = Yes
boolean menuReprint = true;
boolean deBounce = false;
int lastButtonCount = 0;
int lastButton = 0;
int buttonScaleFactor = 250;
boolean inputDetect = false;
int menuLocation = 0;
boolean displaysOn = true;

//Timers
unsigned long timerOne = 0;     //Button Buffer
unsigned long timerTwo = 0;     //Alarm Seconds Timer
unsigned long timerThree = 0;   //Alarm Strobe Timer
unsigned long timerFour = 0;    //Clock timer
unsigned long timerFive = 0;    //Lamp ripple
boolean strobe = false;


void setup() {

  pinMode(buttonDisplay, INPUT);
  pinMode(buttonLightType, INPUT);
  pinMode(buttonYes, INPUT);
  pinMode(buttonNo, INPUT);
  pinMode(buttonDown, INPUT);
  pinMode(buttonUp, INPUT);
  pinMode(alive, OUTPUT);
  

  
  
  pixels.begin();
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(0,0,0)); // Moderately bright blue color.
  }
  pixels.show();


  
  // set up the LCD's number of columns and rows: 
  lcd.begin(20,4);   
  lcd2.begin(20,4);
  
  lcd.backlight();
  lcd2.backlight();
  
  lcd.createChar(0, upArrow);
  lcd.createChar(1, downArrow);
  lcd.createChar(2, love);
  lcd2.createChar(0, upArrow);
  lcd2.createChar(1, downArrow);
  lcd2.createChar(2, love);
  
  lcd.setCursor(0, 0);
  /* Time difference testing
  SimpleTime result;
  //timeTill(int hourCurrent, int minutesCurrent, int hourTrigger, int minutesTrigger)
  result = timeTill(22, 0, 8, 0);
  result = timeTill(1, 30, 8, 0);
  result = timeTill(4, 59, 1, 0);
  */
  
  //Update/Reset system time
  //setDS3231time(00, 28, 18, 6, 14, 11, 15);
  //setDS3231time(00, 35, 23, 7, 25, 9, 16);
/*
  //Add time testing
  //void addTime(int hourAdd, int minuteAdd, int hourCurrent, int minuteCurrent, int secondCurrent) {
  addTime (1, 20, 0);
  addTime (0, 8, 30);
  addTime (16, 45, 0);
  */
  
  //Get time
  updateTime();
  lcd.setCursor(0, 1);
  
  lcd.print("XTELi FW");
  lcd.print("# 002e  "); 

  byte reward = 4;
  writeEEPROMbyte(1, reward);
  lcd.setCursor(0, 2);
  
  lcd.print("Wrote rewards ");
  lcd.print(reward);
  lcd.print(" to ");
  lcd.print(epRewards);

  delay(1000);
  lcd.setCursor(0, 3);
  byte got = 0;
  got = readEEPROMbyte(1);
  lcd.print(got);
  
  delay(3000);  
}

void loop() {
  if (lamp == true) setLamp();
  else if (alarmRunning == false) killLamp();
  
  if (displaysOn == true) {
    lcd.backlight();
    lcd2.backlight();
  } else {
    lcd.noBacklight();
    lcd2.noBacklight();
  }

  if ((alarmOn)&&(runLevel != 1)) {
    mainAlarm();
  }

  if ((timerOn)&&(runLevel == 51)) {
    snoozeAlarm();
  }
  updateLocalTime();
  if (runLevel != 10) displayLocalTime();
    
  buttonInput();

  if (inputBuffer == 5) {
    if (lampStyle == 0) 
      lampStyle = 1;
    else
      lampStyle = 0;
      
    //if (displaysOn == true)
    //  displaysOn = false;
    //else
    //  displaysOn = true;
    inputBuffer = 0;
  }
  
  if (runLevel != 51) 
  {
    if (runLevel == 0) mainMenu();
    if (runLevel == 1) alarmMenu();
    if (runLevel == 2) colorMenu();

    if (runLevel == 4) showRewards();
    if (runLevel == 9) settingsMenu();
    if (runLevel == 10) wakeup();
  }
  if (runLevel == 51)
  {
    sleepMode();
    //alarm();
  }
  


}

