void alarmMenu()
{
  if (menuReprint)
  {
    menuReprint = false;
    lcd.clear();
    lcd2.clear();
    if (menuLocation == 0)
    {      
      lcd.setCursor(0,0);
      lcd.print("Sleep timer preset");
      lcd.setCursor(0,1);
      if (alarmSelection == 0)
      {
        lcd.print(" [2hr] 20min  10min  ");
      }
      if (alarmSelection == 1)
      {
        lcd.print("  2hr [20min] 10min  ");
      }
      if (alarmSelection == 2 ) 
      {
        lcd.print("  2hr  20min [10min] ");
      } 
      
    }


    if (menuLocation == 1) {
      lcd.setCursor(0, 0);
      lcd.print("Snooze - Hour adjust");
      lcd.setCursor(0, 1);      
      lcd.print(fillZero(snoozeReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(snoozeReference.minutes, 2));
    }
    
    if (menuLocation == 2) {
      lcd.setCursor(0, 0);
      lcd.print("Snooze - Minute adjust");
      lcd.setCursor(0, 1);      
      lcd.print(fillZero(snoozeReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(snoozeReference.minutes, 2));
    }
    
    if (menuLocation == 3)
    {
      lcd.setCursor(0, 0);
      lcd.print("Turn on Snooze Timer");
      lcd.setCursor(0, 1);
      if (timerOn == true) { 
        lcd.print("On");
        
      }
      if (timerOn == false) { 
        lcd.print("Off");
      }
    }
    
    if (menuLocation == 4) {
      lcd.setCursor(0, 0);
      lcd.print("Turn on Alarm");
      lcd.setCursor(0, 1);
      if (alarmOn == true) { 
        lcd.print("On");
      }
      if (alarmOn == false) { 
        lcd.print("Off");
      }
    }

    if (menuLocation == 5) {
      lcd.setCursor(0, 0);
      lcd.print("Alarm - Hour adjust");
      lcd.setCursor(0, 1);      
      lcd.print(fillZero(alarmReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(alarmReference.minutes, 2));
    }
    
    if (menuLocation == 6) {
      lcd.setCursor(0, 0);
      lcd.print("Alarm - Minute adjust");
      lcd.setCursor(0, 1);      
      lcd.print(fillZero(alarmReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(alarmReference.minutes, 2));
    }
    
    if (menuLocation == 7)
    {
      lcd.setCursor(0, 0);
      lcd.print("Return to Home");
      lcd.setCursor(2, 1);
      lcd.write((uint8_t)1);
      lcd.print(" - Home Menu ");

    }

  }
 
  if (inputBuffer > 0)
  {
    if (menuLocation == 0)
    {
      if (inputBuffer == 3) {
        alarmSelection++;
        if (alarmSelection == 3) alarmSelection = 0;
        if (alarmSelection == 0) {
          snoozeReference.hours = 2;
          snoozeReference.minutes = 0;
          snoozeReference.seconds = 0;
        }
        if (alarmSelection == 1) {
          snoozeReference.hours = 0;
          snoozeReference.minutes = 20;
          snoozeReference.seconds = 0;
        }
        if (alarmSelection == 2) {
          snoozeReference.hours = 0;
          snoozeReference.minutes = 10;
          snoozeReference.seconds = 0;
        }
        timerOn = false;
      }
    }

    if (menuLocation == 1) {
      if (inputBuffer == 2) {
        snoozeReference.hours+=1;
        if (snoozeReference.hours > 23) snoozeReference.hours = 0;
      }
      if (inputBuffer == 3) {
        snoozeReference.hours-=1;
        if (snoozeReference.hours < 0) snoozeReference.hours = 23;
      }
    }     

    if (menuLocation == 2)
    {
      if (inputBuffer == 2)
      {
        snoozeReference.minutes+=1;
        if (snoozeReference.minutes > 59) snoozeReference.minutes = 0;
      }
      if (inputBuffer == 3)
      {
        snoozeReference.minutes-=1;
        if (snoozeReference.minutes < 0) snoozeReference.minutes = 59;
      }      
    }
    
    if (menuLocation == 3)
    {
      if (inputBuffer == 3) 
      {
        if (timerOn == false) {
          timerOn = true;
        } else timerOn = false;
      }
    }

    if (menuLocation == 4)
    {
      if (inputBuffer == 3) 
      {
        if (alarmOn == false) {
          alarmOn = true;
        } else alarmOn = false;
        
      }
    }


    if (menuLocation == 5)
    {
      if (inputBuffer == 2)
      {
        alarmReference.hours+=1;
        if (alarmReference.hours > 23) alarmReference.hours = 0;
      }
      if (inputBuffer == 3)
      {
        alarmReference.hours-=1;
        if (alarmReference.hours < 0) alarmReference.hours = 23;
      }      
    }

    
    if (menuLocation == 6)
    {
      if (inputBuffer == 2)
      {
        alarmReference.minutes+=1;
        if (alarmReference.minutes > 59) alarmReference.minutes = 0;
      }
      if (inputBuffer == 3)
      {
        alarmReference.minutes-=1;
        if (alarmReference.minutes < 0) alarmReference.minutes = 59;
      }      
    }

    
    if (menuLocation == 7)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 0;
      }
    }
    
    
    
    
        //Move through menus.
    if (inputBuffer == 1)
    {      
      menuLocation--;
      if (menuLocation < 0) menuLocation = 0;
    }
    
    if (inputBuffer == 4)
    {
      menuLocation++;
      
      //Menu length for Control Points  
      if (menuLocation > 7) menuLocation = 7;
    }
    
    menuReprint = true;
    inputBuffer = 0; 
  }
}
