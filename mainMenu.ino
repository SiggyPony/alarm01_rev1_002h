void mainMenu()
{
  if (menuReprint)
  {
    menuReprint = false;
    lcd.clear();
    if (menuLocation < 2) specialMessageOne();
    if (menuLocation == 0)
    {
      lcd.setCursor(0,0);
      
      lcd.print("[Sleep]       Alarm ");
      lcd.setCursor(0,1);
      lcd.print(" Light        Tasks ");
      lcd.setCursor(0,2);
      lcd.print(" Rewards   Settings ");
    }
    
    //Menu for Control Points   
    if (menuLocation == 1)
    {
      lcd.setCursor(0,0);
      lcd.print(" Sleep       [Alarm]");
      lcd.setCursor(0,1);
      lcd.print(" Light        Tasks ");
      lcd.setCursor(0,2);
      lcd.print(" Rewards   Settings ");
    }

    if (menuLocation == 2)
    {
      lcd.setCursor(0,0);
      lcd.print(" Sleep        Alarm ");
      lcd.setCursor(0,1);
      lcd.print("[Light]       Tasks ");
      lcd.setCursor(0,2);
      lcd.print(" Rewards   Settings ");
    }

    if (menuLocation == 3)
    {
      lcd.setCursor(0,0);
      lcd.print(" Sleep        Alarm ");
      lcd.setCursor(0,1);
      lcd.print(" Light       [Tasks]");
      lcd.setCursor(0,2);
      lcd.print(" Rewards   Settings ");
    }
    
    if (menuLocation == 4)
    {
      lcd.setCursor(0,0);
      lcd.print(" Sleep        Alarm ");
      lcd.setCursor(0,1);
      lcd.print(" Light        Tasks ");
      lcd.setCursor(0,2);
      lcd.print("[Rewards]  Settings ");
    }

    if (menuLocation == 5)
    {
      lcd.setCursor(0,0);
      lcd.print(" Sleep        Alarm ");
      lcd.setCursor(0,1);
      lcd.print(" Light        Tasks ");
      lcd.setCursor(0,2);
      lcd.print(" Rewards  [Settings]");
    }

    if (menuLocation == 6)
    {
      lcd.setCursor(0,0);
      lcd.print(" Sleep        Alarm ");
      lcd.setCursor(0,1);
      lcd.print(" Light        Tasks ");
      lcd.setCursor(0,2);
      lcd.print(" Rewards   Settings ");
      lcd.setCursor(0,3);
      lcd.print("[Special Message]   ");
    }
    
  }
 
  if (inputBuffer > 0)
  {
    if (menuLocation == 0)
    {
      if (inputBuffer == 3) 
      {
        lcd.clear();
        lcd2.clear();
        menuLocation = 0;
        lamp = true;
        runLevel = 51;
      }
    }
    
    
    if (menuLocation == 1)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 1;
      }
    }
    
    if (menuLocation == 2)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 2;
      }
    }

    if (menuLocation == 4)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 4;
      }
    }

    if (menuLocation == 5)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 9;
      }
    }

    if (menuLocation == 6)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 10;
      }
    }
    
    
    //Move through menus.
    if (inputBuffer == 1)
    {      
      menuLocation--;
      if (menuLocation < 0) menuLocation = 0;
    }
    
    if (inputBuffer == 4)
    {
      menuLocation++;
      
      //Menu length for Control Points  
      if (menuLocation > 6) menuLocation = 6;
    }
    
    menuReprint = true;
    inputBuffer = 0; 
  }
}
