void sleepMode() {
  
  //if (menuReprint) {  
    lcd.setCursor(0,0);
    if (alarmOn) {
      lcd.print("A ");
      lcd.print(fillZero(alarmReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(alarmReference.minutes, 2));   
    } else {
      lcd.print("       ");
    }
    lcd.print("  ");
    if (timerOn) {
      lcd.print("Sz ");
      lcd.print(fillZero(snoozeReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(snoozeReference.minutes, 2)); 
      lcd.print(":");
      lcd.print(fillZero(snoozeReference.seconds, 2)); 
      
    } else {
      lcd.print("           ");
    }
    specialMessageTwo(); 

    
   // menuReprint = false;  
  //}
  
  if (inputBuffer > 0)
  {
    if (inputBuffer == 2)
    {        
      lightingColor+=1;
      if (lightingColor > 9) lightingColor = 9;
    }
    if (inputBuffer == 3)
    {
      lightingColor-=1;
      if (lightingColor < 0) lightingColor = 0;
    }  
    if (inputBuffer == 4)
    {        
      lightingStrength+=10;
      if (lightingStrength > 100) {
        lightingStrength = 100;
      }
    }
    if (inputBuffer == 1)
    {
      lightingStrength-=10;
      if (lightingStrength < -10) lightingStrength = -10; 
      if (lightingStrength == -10) {
        lightingStrength = 0;
        menuLocation = 0;
        runLevel = 0;
        lcd.clear();
        lcd2.clear();
      }    
    }   
    menuReprint = true;
    inputBuffer = 0;     
  }
/*
    if (duration <= 0) {
      duration = 28800;
      alarmRunning = false;
      menuReprint = true;
      menuLocation = 0;
      runLevel = 10;
      toneAC();
    }    
*/
  inputBuffer = 0;
}
