void settingsMenu()
{
  if (menuReprint)
  {
    menuReprint = false;
    lcd.clear();
    lcd2.clear();
    if (menuLocation == 0)
    {      
      lcd.setCursor(0,0);
      lcd.print("Update system time");
      lcd.setCursor(0,1);
      lcd.print("Time - Hour adjust");
      lcd.setCursor(0, 2);      
      lcd.print(fillZero(timeUpdateReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(timeUpdateReference.minutes, 2));
      
    }


    if (menuLocation == 1) {
      lcd.setCursor(0,0);
      lcd.print("Update system time");
      lcd.setCursor(0,1);
      lcd.print("Time - Minute adjust");
      lcd.setCursor(0, 2);      
      lcd.print(fillZero(timeUpdateReference.hours, 2));
      lcd.print(":");
      lcd.print(fillZero(timeUpdateReference.minutes, 2));
    }
    
    if (menuLocation == 2) {
      lcd.setCursor(0,0);
      lcd.print("Update system time");
      lcd.setCursor(0, 1);
      lcd.print("Set new time"); 
      lcd.setCursor(0, 2);
      lcd.write((uint8_t)1);
      lcd.print(" - Set system time");
    }
    
    if (menuLocation == 3)
    {
      lcd.setCursor(0, 0);
      lcd.print("Return to Home");
      lcd.setCursor(2, 1);
      lcd.write((uint8_t)1);
      lcd.print(" - Home Menu ");
    }

  }
 
  if (inputBuffer > 0)
  {
    if (menuLocation == 0) {
      if (inputBuffer == 2) {
        timeUpdateReference.hours+=1;
        if (timeUpdateReference.hours > 23) timeUpdateReference.hours = 0;
      }
      if (inputBuffer == 3) {
        timeUpdateReference.hours-=1;
        if (timeUpdateReference.hours < 0) timeUpdateReference.hours = 23;
      }
    }     

    if (menuLocation == 1)
    {
      if (inputBuffer == 2)
      {
        timeUpdateReference.minutes+=1;
        if (timeUpdateReference.minutes > 59) timeUpdateReference.minutes = 0;
      }
      if (inputBuffer == 3)
      {
        timeUpdateReference.minutes-=1;
        if (timeUpdateReference.minutes < 0) timeUpdateReference.minutes = 59;
      }      
    }
    
    if (menuLocation == 2)
    {
      if (inputBuffer == 3) 
      {
        setDS3231time(timeUpdateReference.seconds, timeUpdateReference.minutes, timeUpdateReference.hours, timeReference.dayWeek, timeReference.dayMonth, timeReference.month, timeReference.year);
      }
    }    
    if (menuLocation == 3)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 0;
      }
    }  
       
    //Move through menus.
    if (inputBuffer == 1)
    {      
      menuLocation--;
      if (menuLocation < 0) menuLocation = 0;
    }
    
    if (inputBuffer == 4)
    {
      menuLocation++;
      
      //Menu length for Control Points  
      if (menuLocation > 3) menuLocation = 3;
    }
    
    menuReprint = true;
    inputBuffer = 0; 
  }
}
