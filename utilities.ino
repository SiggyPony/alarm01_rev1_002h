String fillZero(int tempValue, int fillNum)
{  
  String returnString = "";
  if ((fillNum > 4)&&(tempValue < 10000)) returnString += "0";
  if ((fillNum > 3)&&(tempValue < 1000)) returnString += "0";
  if ((fillNum > 2)&&(tempValue < 100)) returnString += "0";
  if ((fillNum > 1)&&(tempValue < 10)) returnString += "0";
  returnString += (String)tempValue;
  return returnString;
}  

String fillZeroLong(unsigned long tempValue, int fillNum)
{  
  if ((fillNum > 4)&&(tempValue < 10000)) lcd.print(F("0"));
  if ((fillNum > 3)&&(tempValue < 1000)) lcd.print(F("0"));
  if ((fillNum > 2)&&(tempValue < 100)) lcd.print(F("0"));
  if ((fillNum > 1)&&(tempValue < 10)) lcd.print(F("0"));
  return (String)tempValue;
}  

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}

// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}

void buttonInput()
{
  if (deBounce == false)
  {
    digitalWrite(alive, LOW);
    if (digitalRead(buttonNo))
    {
      if (lastButton == 1)
      {
        lastButtonCount++;
      }
      else
      {
        lastButtonCount = 1;
      }
      inputBuffer = 1;
      inputDetect = true;
      lastButton = 1;
    }
    if (digitalRead(buttonUp))
    {  
      if (lastButton == 2)
      {
        lastButtonCount++;
      }
      else
      {
        lastButtonCount = 1;
      }
      inputBuffer = 2;
      inputDetect = true;
      lastButton = 2;
    }
    if (digitalRead(buttonDown))
    {
      if (lastButton == 3)
      {
        lastButtonCount++;
      }
      else
      {
        lastButtonCount = 1;
      }
      inputBuffer = 3;
      inputDetect = true;
      lastButton = 3;
    }
    if (digitalRead(buttonYes))
    { 
      if (lastButton == 4)
      {
        lastButtonCount++;
      }
      else
      {
        lastButtonCount = 1;
      }
      inputBuffer = 4;
      inputDetect = true;
      lastButton = 4;
    }   
    if (digitalRead(buttonDisplay))
    { 
      if (lastButton == 5)
      {
        lastButtonCount++;
      }
      else
      {
        lastButtonCount = 1;
      }
      inputBuffer = 5;
      inputDetect = true;
      lastButton = 5;
    }  
    if (digitalRead(buttonLightType))
    { 
      if (lastButton == 6)
      {
        lastButtonCount++;
      }
      else
      {
        lastButtonCount = 1;
      }
      inputBuffer = 6;
      inputDetect = true;
      lastButton = 6;
    }  
    
    if (inputDetect == true)
    {
      deBounce = true;
      timerOne = millis();
      if (lastButtonCount > 20)
      {
        buttonScaleFactor = 50;
      }
      else if (lastButtonCount > 10)
      {
        buttonScaleFactor = 150;
      }
      else 
      {
        buttonScaleFactor = 250;
      }
      inputDetect = false;
    }
    else
    {
      lastButton = 0;
    }
  }
  else
  {
    digitalWrite(alive, HIGH);
    if (millis() > (timerOne + buttonScaleFactor))
    {
      deBounce = false;      
    }
  }
}

//TODO add a nicer alarm pre strobe?

void alarmStrobe() {
        if (millis() > (timerThree + 250))
      {
        timerThree = millis();
        if (strobe == true) strobe = false;
        else strobe = true;
        
      }
      if (strobe == true) {
        for(int i=0;i<NUMPIXELS;i++){
          pixels.setPixelColor(i, pixels.Color(255,0,0)); // Moderately bright blue color.
        }
        pixels.show();
        toneAC(600);
      } else {
        for(int i=0;i<NUMPIXELS;i++){
          pixels.setPixelColor(i, pixels.Color(0,0,0)); // Moderately bright blue color.
        }
        pixels.show();
        toneAC();
      }
  
}

void setLamp() {
  int tempStrength = 0;
  int newValue = 0;
  if (lightingStrength == -10)
    tempStrength = 0;
  else
    tempStrength = lightingStrength;
  if (lampStyle == 1) {
    if (millis() > (timerFive))
    {
      timerFive = millis();

      // One
      //int newValue = (int)random(0, 100);
      //if (newValue > tempStrength) newValue = tempStrength;
      
      /*
      if (rippleModTwo == 0) {
        if (rippleModOne < 100) rippleModOne+=2; 
        if (rippleModOne >= 100) {
          rippleModTwo = 1;
          rippleModOne = 100;
        }
      }
      if (rippleModTwo == 1) {
        if (rippleModOne > 0) rippleModOne-=2; 
        if (rippleModOne <= 0) {
          rippleModTwo = 0;
          rippleModOne = 0;
        }
      }
      int newValue = rippleModOne;
      if (newValue < 5) newValue = 5;
      */
      
      
    /*      
      int carryValue = 0;
      for (int i = 0; i < NUMPIXELS; i++) {
        carryValue = rippleValue[i];
        rippleValue[i] = newValue;
        newValue = carryValue;
      }
    */



            
      if (rippleModTwo == 0) {
        if (rippleModOne < 100) rippleModOne+=1; 
        if (rippleModOne >= 100) {
          rippleModTwo = 1;
          rippleModOne = 100;
        }
      }
      if (rippleModTwo == 1) {
        if (rippleModOne > 0) rippleModOne-=1; 
        if (rippleModOne <= 0) {
          rippleModTwo = 0;
          rippleModOne = 0;
        }
      }
      newValue = rippleModOne;
      if (newValue < 5) newValue = 5;
    }
    int redS = 0;
    int greenS = 0;
    int blueS = 0;
    for (int i=0;i<NUMPIXELS;i++){
      //redS = (colorsRGB[lightingColor][0]/100)*rippleValue[i];
      //greenS = (colorsRGB[lightingColor][1]/100)*rippleValue[i];
      //blueS = (colorsRGB[lightingColor][2]/100)*rippleValue[i];
      redS = (colorsRGB[lightingColor][0]/100)*newValue;
      greenS = (colorsRGB[lightingColor][1]/100)*newValue;
      blueS = (colorsRGB[lightingColor][2]/100)*newValue;
      pixels.setPixelColor(i, pixels.Color(redS,greenS,blueS)); 
    }
  }
  if (lampStyle==0) {
    for (int i=0;i<NUMPIXELS;i++){
      int redS = (colorsRGB[lightingColor][0]/100)*tempStrength;
      int greenS = (colorsRGB[lightingColor][1]/100)*tempStrength;
      int blueS = (colorsRGB[lightingColor][2]/100)*tempStrength;
      pixels.setPixelColor(i, pixels.Color(redS,greenS,blueS)); 
    }
  }
  pixels.show();
}
  
void killLamp() {
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(0,0,0));
  }
  pixels.show();
}

void specialMessageOne() {
  lcd2.setCursor(0,2);
  lcd2.print(F("Redacted Personal Content :)"));
  lcd2.write((uint8_t)2);
  lcd2.write((uint8_t)2);
}

void specialMessageTwo() {
  lcd.setCursor(0,1);
  lcd.print(F("Remember you're made"));  
  lcd.setCursor(0,2);
  lcd.print(F("of stardust, and are"));
  lcd.setCursor(0,3);
  lcd.print(F("quite litterally a  "));
  lcd2.setCursor(0,1);
  lcd2.print(F("child of the        "));
  lcd2.setCursor(0,2);
  lcd2.print(F("universe."));
  lcd2.setCursor(0,3); 
  lcd2.print(F("Partially Redacted :)"));
}

void updateLocalTime() {
  if ((timerFour + 1000) <= millis()) {
    timerFour = millis();
    updateTime(); 
  }  
}

void displayLocalTime() {
  //Update time display
  lcd2.setCursor(0,0);
  lcd2.print(fillZero(timeReference.hours, 2));
  lcd2.print(":");
  lcd2.print(fillZero(timeReference.minutes, 2));
  lcd2.print(":");
  lcd2.print(fillZero(timeReference.seconds, 2));
  lcd2.print("    ");
  lcd2.print(fillZero(timeReference.dayMonth, 2));
  lcd2.print("-");                       
  lcd2.print(fillZero(timeReference.month, 2));
  lcd2.print("-");
  lcd2.print(fillZero(timeReference.year, 2));
  if (runLevel != 51) {
    lcd2.setCursor(0,1);
    if (alarmOn) {
      lcd2.print("A ");
      lcd2.print(fillZero(alarmReference.hours, 2));
      lcd2.print(":");
      lcd2.print(fillZero(alarmReference.minutes, 2));   
    } else {
      lcd2.print("       ");
    }
    lcd2.print("  ");
    
    if (timerOn) {
      lcd2.print("Sz ");
      lcd2.print(fillZero(snoozeReference.hours, 2));
      lcd2.print(":");
      lcd2.print(fillZero(snoozeReference.minutes, 2)); 
      lcd2.print(":");
      lcd2.print(fillZero(snoozeReference.seconds, 2)); 
    } else {
      lcd2.print("           ");
    }
    
  }
}
/*
SimpleTime timeTill(int hourCurrent, int minuteCurrent, int hourTrigger, int minuteTrigger) {   
  int hourResult = 0;
  int minuteResult = 0;

  SimpleTime returnTime;

  if (hourCurrent > hourTrigger) {
    returnTime.hours = 24 - hourCurrent;
    returnTime.hours += hourTrigger;
  } else {
    returnTime.hours = hourTrigger - hourCurrent;
  }


  if (minuteCurrent > minuteTrigger) {
    returnTime.minutes = 60 - minuteCurrent;
    returnTime.minutes += minuteTrigger;
  } else {
    returnTime.minutes = minuteTrigger - minuteCurrent;
  }

  if (minuteCurrent > minuteTrigger) {
    returnTime.minutes = 60 - minuteCurrent;
    returnTime.minutes += minuteTrigger;
  } else {
    returnTime.minutes = minuteTrigger - minuteCurrent;
  }

  
  lcd.clear();
  lcd2.clear();
  lcd.setCursor(0,0);
  lcd.print(hourCurrent);
  lcd.print(":");
  lcd.print(minuteCurrent);
  lcd.print(" ");
  
  lcd.print(hourTrigger);
  lcd.print(":");
  lcd.print(minuteTrigger);
  lcd.print(" ");

  
  lcd.print(returnTime.hours);
  lcd.print(":");
  lcd.print(returnTime.minutes);
  delay(4000);

  return returnTime;
}
*/
void snoozeTimeUpdate() {
  /*
  snoozeOffset.seconds = 0;
  snoozeOffset.minutes = 0;
  snoozeOffset.hours = 0;
 
  //Calculate and update the new countdown time
  snoozeOffset.seconds += (timeReference.seconds + snoozeReference.seconds);
  if (snoozeOffset.seconds > 60) {
    snoozeOffset.seconds -= 60;
    snoozeOffset.minutes++;
  }
  
  snoozeOffset.minutes += (timeReference.minutes + snoozeReference.minutes);
  if (snoozeOffset.minutes > 60) {
    snoozeOffset.minutes -= 60;
    snoozeOffset.hours++;
  }
  
  snoozeOffset.hours += (timeReference.hours + snoozeReference.hours);
  if (snoozeOffset.hours > 24) {
    snoozeOffset.hours -= 24;
  }
  */
  
  //Check if time has passed and if so how much.
  

 
/*
  lcd.clear();
  lcd2.clear();
  lcd.setCursor(0,0);
  lcd.print(timeReference.hours);
  lcd.print(":");
  lcd.print(timeReference.minutes);
  lcd.print(":");
  lcd.print(timeReference.seconds);
  lcd.print(" ");

  lcd.print(snoozeReference.hours);
  lcd.print(":");
  lcd.print(snoozeReference.minutes);
  lcd.print(":");
  lcd.print("00");
  lcd.print("  ");

  lcd.print(returnTime.hours);
  lcd.print(":");
  lcd.print(returnTime.minutes);
  lcd.print(":");
  lcd.print(returnTime.seconds);
  
  delay(10000);
  return returnTime;
*/ 
}


































  
