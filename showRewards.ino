void showRewards()
{
  if (menuReprint)
  {
    menuReprint = false;
    lcd2.clear();
    
    lcd2.setCursor(0,1);
    lcd2.write((uint8_t)2);
    lcd2.print(" Rewards ");
    lcd2.write((uint8_t)2);
    lcd2.setCursor(0,2);
    lcd2.print("Siggys Points ");
    lcd2.print(readEEPROMbyte(epRewards));


  }
 
  if (inputBuffer > 0)
  {
    if (menuLocation == 0)
    {
      if (inputBuffer == 3) 
      {
        menuLocation = 0;
        runLevel = 0;
      }
    }
    
    
    
    /*
    //Move through menus.
    if (inputBuffer == 1)
    {      
      menuLocation--;
      if (menuLocation < 0) menuLocation = 0;
    }
    
    if (inputBuffer == 4)
    {
      menuLocation++;
      
      //Menu length for Control Points  
      if (menuLocation > 3) menuLocation = 3;
    }
    */    
    menuReprint = true;
    inputBuffer = 0; 
  }
}
